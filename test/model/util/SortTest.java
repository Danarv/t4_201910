package model.util;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import model.util.Sort;

public class SortTest extends TestCase{

	// Muestra de datos a ordenar
	private Comparable[] datos = {7, 10, 4, 1, 15};
	
	private Comparable[] aux = new Comparable [5];
	
	private Sort sortSample;
	
	@Before
	public void setUp() throws Exception{
		System.out.println("Codigo de configuracion de muestra de datos a probar");
		sortSample = new Sort();
	}
	
	public void setUp2() throws Exception{
		System.out.println("Codigo de configuracion de muestra de datos a probar");
		sortSample = new Sort();
		aux[0] = 1;
		aux[1] = 4;
		aux[2] = 7;
		aux[3] = 10;
		aux[4] = 15;
	}

	@Test
	public void testMerge() throws Exception {
		setUp();
		System.out.println("datos sin arreglar" + Arrays.toString(datos));
		sortSample.ordenarMergeSort(datos, aux);
		System.out.println("datos arreglados" + Arrays.toString(datos));
		assertTrue(datos.equals(aux));
	}
	
	public void testQuick() throws Exception {
		setUp2();
		System.out.println("datos sin arreglar" + Arrays.toString(datos));
		sortSample.ordenarQuickSort(datos, 0, datos.length);
		System.out.println("datos arreglados" + Arrays.toString(datos));
		assertTrue(datos.equals(aux));
	}
	
	public void testShell() throws Exception {
		setUp2();
		System.out.println("datos sin arreglar" + Arrays.toString(datos));
		sortSample.ordenarShellSort(datos);
		System.out.println("datos arreglados" + Arrays.toString(datos));
		assertTrue(datos.equals(aux));
	}
}
